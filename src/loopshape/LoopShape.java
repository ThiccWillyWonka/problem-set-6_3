package loopshape;

public class LoopShape {
    
    static void createRectangle(int width, int height){
    if (width<1 || height<1){
        System.out.println("Width and height can't be less than one");
        return;
     }
    if (width == 1 && height == 1) {
        System.out.println("#");
        return;
    }
    for(int i = 0; i<width;i++){
        System.out.print("#");
    }
    System.out.println(" ");
    for (int i = 1; i<(height-1);i++){
        System.out.print("#");
        for (int f = 1; f<(width-1); f++){
            System.out.print(" ");
            }
        System.out.println("#");
        }
    for (int i = 0; i<width;i++){
        System.out.print("#");
        }
    
        System.out.println("");
    }
    
    static void createTriangle(int leg){
        if (leg<1 ){
            System.out.println("Leg can't be less than one");
            return;
        }
        if (leg == 1 ) {
            System.out.println("#");
            return;
        }
        for(int i = 1; i <= leg-1; i++){
            System.out.print("#");  
            if (i == 1){ 
              System.out.println(" "); 
              continue;
            }
            if (i == 2){
              System.out.println("#");
              continue;
            }
            for (int f = 2; f<i;f++){
                System.out.print(" ");
            }
            System.out.println("#");
        }  
        for (int i = 0; i<leg; i++){
            System.out.print("#");
        }
        System.out.println(" ");
    }
}
